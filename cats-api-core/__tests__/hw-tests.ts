import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';

const cats: CatMinInfo[] = [{ name: 'Тестовыйкотикзайчик', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('API core', () => {
    beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            if ((add_cat_response.body as CatsList).cats[0].id) {
                catId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
            throw new Error('Не удалось создать котика для автотестов!');
        }
    });

    afterAll(async () => {
        await HttpClient.delete(`core/cats/${catId}/remove`, {
            responseType: 'json',
        });
    });

    it('Найти котика по id', async () => {
        const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toEqual({
            cat: Object ({
            id: catId,
            ...cats[0],
            dislikes: expect.any(Number),
            likes: expect.any(Number),
            tags: null,
        })
        });
    });

    it('Найти котика по неккоректному id', async () => {
        try {
            const response = await HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
                responseType: 'json',
            });
            expect(response.statusCode).toBe(400)
        } catch (error) {
        }
    });


    it('Изменить описание котика', async () => {
        const response = await HttpClient.post(`core/cats/save-description`, {
            responseType: 'json',
            json: {
                catId: catId,
                catDescription: "Тестовый котик"
                },
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toEqual({
            id: catId,
            name: expect.any(String),
            description: "Тестовый котик",
            gender: expect.any(String),
            tags: null,
            dislikes: expect.any(Number),
            likes: expect.any(Number),
        });
    });


    it('Получение списка котов по группам', async () => {
        const response = await HttpClient.get(`core/cats/allByLetter?order=asc`, {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toEqual({
            groups: expect.arrayContaining([({
                title: expect.any(String),
                cats: expect.arrayContaining([
                    expect.objectContaining({
                        id: expect.any(Number),
                        name: expect.any(String),
                        description: expect.any(String),
                        gender: expect.any(String),
                        dislikes: expect.any(Number),
                        likes: expect.any(Number),
                        tags: null,
                        count_by_letter: expect.any(String)
                    }),
                ]),

                count_in_group: expect.any(Number),
                count_by_letter: expect.any(Number),
            }),
            ]),
            count_output: expect.any(Number),
            count_all: expect.any(Number),
        });
    });

});